#Variable
Y = True
y = True
N = False
n = False
somQPLijst = []
aantalTotaalSP = []

#Vraag de gebruiker voor cijfers
while True:
    cijfer = float(input("Welk cijfer heb je behaald? : "))
    if cijfer <= 5.4: #Controleer als het cijfer hoger dan een 5.4 is
       print("Cijfer moet hoger zijn dan een 5.4, probeer opnieuw")
    elif cijfer > 10:
        print("Cijfer moet lager zijn dan 10, probeer opnieuw")
    else:
        sp = int(input("Aantal studiepunten? : "))
        berekeningQP = cijfer * sp 
        somQPLijst.append(berekeningQP) #Maak een lijst van berekeningQP
        aantalTotaalSP.append(sp) #Een lijst maken van de SP en QP
        doorgaan = input("Wil je nog een cijfer invoeren? (Y/N) : ") #De gebruiker vragen als ze nog een cijfer willen invoeren
        if doorgaan == "N" or "n": 
            break
        else:
            continue

somQP = sum(somQPLijst) #De opgetelde waarde van alle QP
somSP = sum(aantalTotaalSP) #De opgetelde waarde van alle SP
gemiddelde = round(float(somQP / somSP), 1)

#Berekenen GPA
if (gemiddelde >= 8):
    print("Je GPA is 4")
elif (gemiddelde >= 7.7):
    print("Je GPA is 3.7")
elif (gemiddelde >= 7.4):
    print("Je GPA is 3.3")
elif (gemiddelde >= 7):
    print("Je GPA is 3")
elif (gemiddelde >= 6.7):
    print("Je GPA is 2.7")
elif (gemiddelde >= 6.4):
    print("Je GPA is 2.3")
elif (gemiddelde >= 6):
    print("Je GPA is 2")
elif (gemiddelde >= 5.6):
    print("Je GPA is 1.7")
elif (gemiddelde >= 5.4):
    print("Je GPA is 1.3")
else:
    print("Je GPA is lager dan 1.3")